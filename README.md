# HiPSane

Sanity checker for hipslists. This tool simply checks if all HiPSes linked in the hipslist have keys that are consistent with the keys in the hipslist.

# Usage
The script is fairly simple and has no complex dependencies. I have used it on my laptop with python 3.9 installed. The script has a `--help` flag. Usage is simply by running `hipsane https://my_hipsurl.nl` where the URL should be to the url where the hipslist is hosted. The script raises an exception if an error is found in the configuration.

# HiPSLint
In principle this functionality will be implemented in a future version of [hipslint (jar file)](http://aladin.u-strasbg.fr/hips/Hipslint.jar). However as lng as this is not the case, you can use this. I do hoever not expect to do much further development on the code.

# Pipeline
This repository hosts the pipeline that is regularly executed to check our HiPS service for inconsistencies. 
