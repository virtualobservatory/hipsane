#! /usr/bin/env python3
__author__ = "Yan Grange"
__version__ = "0.1.0"
__license__ = "Apache 2.0"

import logging
import argparse
from urllib.parse import urlsplit
# Looks dirty but does the actual joining better, see:
# https://stackoverflow.com/questions/1793261/how-to-join-components-of-a-path-when-you-are-constructing-a-url-in-python
from posixpath import join as urljoin 
import requests

#logging.basicConfig(encoding='utf-8', level=logging.DEBUG)
logger = logging.getLogger(__name__)

def read_keyvals(data_content):
    """Read key value list, with "=" signs. Also skip comments
       using the "#" character (both inline as full line)

    Parameters
    ----------
    data_content : list(str)
        List of lines to parse. 

    Returns
    -------
    list(tuple)
        List of (key, value) tuples.
    """
    logger.debug("parsing key-value pairs.")
    keyvallist = list()
    for line in data_content:
        if line.strip().startswith("#"):
            logger.debug(f"Skipping line {line}")
            pass
        line = line.split("#",1)[0]
        keyval = line.split("=",1)
        if len(keyval) < 2:
            logger.debug(f"Skipping entry {keyval} because it is too short.")
            continue
        logger.debug(f"Extracting Key value pair: {keyval}")
        keyvallist.append(keyval)
    return keyvallist
    
def parse_hipslist(kvpairs):
    """Parse hipslist to dict. The output will be a list of dicts, split by the
       creator_did field of the hipslist entry and the keys of
       the dicts are the key-value pairs in the hipslist.

    Parameters
    ----------
    kvpairs : list(tuple)
        list of key-value pair tuples from hipslist.

    Returns
    -------
    list(dict)
        list of dicts, as described above.
    """
    logger.debug("Parsing hipslist do dict of dicts.")
    hipses = list()
    for entry in kvpairs:
        if entry[0].strip() == "creator_did":
            logger.debug("creator_did entry: {mydid}")
            hipses.append(dict())
        entkey = entry[0].strip()
        entval = entry[1].strip()
        logger.debug(f"Adding key: {entkey}, value: {entval} to the dict.")
        hipses[-1][entkey] = entval
    return hipses


def add_hipslist_to_url(url):
    """
    If the URL does not end with /hipslist of /hipslist.txt, append /hipslist (using urljoin)
    Parameters
    ----------
    url : str
        HiPS service URL
    
    Returns
    -------
    str
        HiPS service hipslist URL
    """
    logger.debug(f"Parsing URL {url}")
    if not urlsplit(url).scheme:
        logger.error("No scheme provided with the URL. Please start with probably http or https.")
        raise ValueError("No scheme provided")
    if not (url.rsplit("/",1)[1] == "hipslist" or url.rsplit("/",1)[1] == "hipslist.txt"):
        url = urljoin(url, "hipslist")
        logger.debug(f"URL with hipslist is: {url}")
    return url

def get_kv_from_url(url):
    """Get key-value pairs from a URL

    Parameters
    ----------
    url : str
        The URL to read the key-value pairs from

    Returns
    -------
    list(tuple)
        List of (key, value) tuples (see read_keyvals for description).
    """
    logger.debug(f"Getting data from {url}")
    with requests.Session() as sess:
        urlresp = sess.get(url)
        urlresp.raise_for_status()
        urldata = urlresp.content.decode('utf-8').split("\n")
    logger.debug(f"URL data: {urldata}")
    return read_keyvals(urldata)

def checkhips(hl_entry):
    """Check if the hipslist is consistent with the HiPS properties of the entries

    Parameters
    ----------
    hl_entry : str
        HiPS entry on the hipslist to check the parameters for.

    Raises
    ------
    ValueError
        If any of the HiPSes have inconsistent data, this is raised.
    """
    creator_did = hl_entry["creator_did"]
    logger.info(f"Checking {creator_did}")
    url = hl_entry['hips_service_url']
    url = urljoin(url, "properties")
    logger.debug(f"URL to check {url}")
    logger.debug(f'Reading from URL: {url}')
    hips_content = get_kv_from_url(url)
    raising = False
    for ct in hips_content:
        ct_key = ct[0].strip()
        ct_val = ct[1].strip()
        if ct_key in hl_entry.keys():
            if hl_entry[ct_key] != ct_val:
                logger.error(f"Error, {ct_key} is {hl_entry[ct_key]} and not {ct_val} for {creator_did}")
                raising = True
    if raising:
        raise ValueError("There was an error with the HiPS.")


def main(args):
    """ Main entry point of the app """
    url = add_hipslist_to_url(args.URL)
    logger.info(f"Getting hipslist from {url}")
    kvpairs = get_kv_from_url(url)
    hipslist_data = parse_hipslist(kvpairs)
    for hipslist_entry in hipslist_data:
        checkhips(hipslist_entry)
    

if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser(description=f"Version: {__version__} Sanity check if the hipslist of a his service contains the same data as the underlying HiPSes.")

    # Required positional argument
    parser.add_argument("URL", help="HiPS service to check")

    # Specify output of "--version"
    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s (version {version})".format(version=__version__))

    args = parser.parse_args()
    main(args)